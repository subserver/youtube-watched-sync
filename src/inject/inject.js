/**
 * Syned YouTube Watch History
 * 
 * @author Ryan Tiedemann <ryan@simplexmedia.co.nz>
 * @author David Campbell
 * 
 * 1. Setup IndexDB to store watched videos.
 * 2. Sync List with chrome.storage.sync API.
 * 3. Mark videos as watched on youtube website.
 */

YouTubeWatchedDB.open();

chrome.extension.sendMessage({}, function(response) {
	var readyStateCheckInterval = setInterval(function() {
		if (document.readyState === "complete") {
			clearInterval(readyStateCheckInterval);
			// Page Load Done.

			// ----------------------------------------------------------
			// This part of the script triggers when page is done loading
			console.log("Hello. This message was sent from scripts/inject.js");
			// ----------------------------------------------------------


			chrome.extension.sendMessage({load: true}, function(response) {
				console.log("Response", response);
			});
		}
	}, 10);
});