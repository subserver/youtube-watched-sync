/**
 * @class YouTubeWatchedDB
 * Store information about watched videos.
 */
(function() {
    'use strict';
    
    /**
     * Ensure a condition is met, else error.
     * @param {*} condition 
     * @param {string} message 
     */
    function assert(condition, message = "Assert Failed") {
        if (!condition) {
            console.error(message, condition);
            throw new Error(message);
        }
    }

    /**
     * Constructor.
     */
	function YouTubeWatchedDB() {
        this.db = null;
        this.open().catch(function(errorEvent) {
            console.error("Failed to connect to IndexedDB", errorEvent);
        });
	}
    
    /**
     * Open connection to IndexedDB.
     */
	YouTubeWatchedDB.prototype.open = function() {
        var self = this;
		return new Promise(function(resolve, reject){
			var openRequest = window.indexedDB.open("YouTubeWatchedHistory", 1);
			openRequest.onsuccess = function(event) {
                console.log("Successfully connected to IndexDB");
                self.db = openRequest.result;
				resolve(self);
			};

			openRequest.onerror = function(event) {
				reject(event);
			};

			openRequest.onupgradeneeded = function(event) {
				let videoStore = event.currentTarget.result.createObjectStore("videos", {keyPath: "id"});
			};
		})
    };
    
    /**
     * Mark a video as watched.
     */
    YouTubeWatchedDB.prototype.add = function(videoID, data) {
        assert(this.db, "Use open before attempting to add videos");
        assert(videoID, "You must provide a videoID");
        data = data || {};
        data.id = videoID;

        return new Promise(function(resolve, reject) {
            var transaction = db.transaction("videos", IDBTransaction.READ_WRITE);
            var store = transaction.objectStore("videos");
            store.add(data);

            transaction.onsuccess = function() {
                resolve();
            };

            transaction.onerror = function() {
                reject();
            };
        });
    };

    /**
     * Filter a set of video ids returning only 
     * @param {array} videoIDs
     */
    YouTubeWatchedDB.prototype.filterWatched = function(videoIDs) {
        return false;
    }

	window.YouTubeWatchedDB = new YouTubeWatchedDB();
})();