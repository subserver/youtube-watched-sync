/**
 * Syned YouTube Watch History
 * 
 * @author Ryan Tiedemann <ryan@simplexmedia.co.nz>
 * @author David Campbell
 * 
 * 1. Setup IndexDB to store watched videos.
 * 2. Sync List with chrome.storage.sync API.
 * 3. Mark videos as watched on youtube website.
 * 
 * References:
 * - Chrome Sync API https://developers.chrome.com/extensions/storage
 * 
 */


 /**
  * Register first installation history scrape.
  */
 chrome.runtime.onInstalled.addListener(function() {
     console.log("Fresh Install");
     // TODO: Scrape history for youtube videos, mark as watched.
     chrome.history.search({text: "youtube.com"}, function(visited) {
        console.log(visited);
     });
 });
 
 /**
  * Register Message Listener.
  */
 chrome.runtime.onMessage.addListener(
   function(request, sender, sendResponse) {
     if (request.save) {
       let videos = request.videos;
       // TODO: Save videos into sync API.
       sendResponse({success: true});
       return true;
     } else if (request.load) {
       let videos = new Set(); // TODO: Load videos from sync API.
       sendResponse({success: true, videos: videos});
       return true;
     }
     sendResponse({success: false, error: "Unknown Request Method"});
  });